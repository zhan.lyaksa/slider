# Slider

A simple slider.

* Works on mobile and desktop devices
* Supports swipes
* Works with any HTML content
* Supports multiple slides on screen
* Supports infinite option
* Supports scrolling to a selected slide

## Installation

1. Clone or download the repository, unzip if necessary.
2. Open repository directory in Command Prompt.
3. Run `npm install`
4. Run `npm start`
5. Slider demo page should be available at http://localhost:3000

## Usage

```
<Slider
    slidesToShow={number}
    slidesToScroll={number}
    infinite={boolean}
    goToSlide={number}
>
    <div>your item 1</div>
    <div>your item 2</div>
    <div>your item 3</div>
</Slider>
```

* `slidesToShow` - a number of slides to display on a screen. Required.
* `slidesToScroll` - a number of slides to scroll using arrows. Required.
* `infinite` - tells whether slider is infinite or not. Optional. Default `false`.
* `goToSlide` - gives ability to move to a specific slide. Optional. Default `null`.

Slider compares slide count and `slidesToShow`, `slidesToScroll` values in order to prevent incorrect use when an user is trying to display or scroll more slides than he has.
