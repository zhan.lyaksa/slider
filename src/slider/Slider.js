import React, { useCallback, useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import { isMobile } from '../utils';
import './Slider.css';

const repeatArray = (arr, n) => {
    const result = [];

    while (result.length < n) {
        result.push(...arr);
    }

    result.length = n;

    return result;
};

const Slider = ({
    children,
    slidesToShow,
    slidesToScroll,
    infinite,
    goToSlide
}) => {
    const [sliderWidth, setSliderWidth] = useState(0);
    const [slideWidth, setSlideWidth] = useState(0);
    const [currentSlide, setCurrentSlide] = useState(0);
    const [scrollPosition, setScrollPosition] = useState(0);
    const [destinationSlide, setDestinationSlide] = useState(goToSlide);
    const [initialDragX, setInitialDragX] = useState(null);
    const [dragX, setDragX] = useState(null);
    const [disableTransition, setDisableTransition] = useState(false);
    const slideContainerRef = useRef();
    const slideCount = children.length;
    const slidesToAppend = infinite
        ? Math.max(slidesToScroll, slidesToShow)
        : 0;
    const isMobileLayout = isMobile();

    const moveToSlide = useCallback(
        (slide, noTransition = false) => {
            if (!infinite) {
                if (slide < 0) {
                    slide = 0;
                } else if (slide + slidesToShow > slideCount) {
                    slide = slideCount - slidesToShow;
                }
            }

            const position =
                scrollPosition + (currentSlide - slide) * slideWidth;

            if (noTransition) {
                setDisableTransition(true);
            }

            setCurrentSlide(slide);
            setScrollPosition(position);

            if (noTransition) {
                setTimeout(() => setDisableTransition(false));
            }
        },
        [
            slideCount,
            currentSlide,
            scrollPosition,
            slideWidth,
            slidesToShow,
            infinite
        ]
    );

    const onLeftArrowClick = useCallback(() => {
        let n;

        if (infinite) {
            n = slidesToScroll;
        } else {
            n =
                currentSlide - slidesToScroll < 0
                    ? currentSlide
                    : slidesToScroll;
        }

        moveToSlide(currentSlide - n);
    }, [currentSlide, slidesToScroll, moveToSlide, infinite]);

    const onRightArrowClick = useCallback(() => {
        const endOfVisibleSlides = currentSlide + slidesToShow;
        let n;

        if (infinite) {
            n = slidesToScroll;
        } else {
            n =
                endOfVisibleSlides + slidesToScroll > slideCount
                    ? slideCount - endOfVisibleSlides
                    : slidesToScroll;
        }

        moveToSlide(currentSlide + n);
    }, [
        currentSlide,
        slidesToScroll,
        slideCount,
        slidesToShow,
        moveToSlide,
        infinite
    ]);

    const onTrackDragStart = useCallback(
        e => {
            if (dragX === null) {
                if (!isMobileLayout) {
                    e.preventDefault();
                }

                let x;

                if (e.touches && e.touches.length) {
                    x = e.touches[0].clientX;
                } else if (e.button === 0) {
                    x = e.clientX;
                }

                setDisableTransition(true);
                setInitialDragX(x);
                setDragX(x);
            }
        },
        [dragX, isMobileLayout]
    );

    const onTrackDrag = useCallback(
        e => {
            if (dragX !== null) {
                let x;

                if (e.touches && e.touches.length) {
                    x = e.touches[0].clientX;
                } else if (e.button === 0) {
                    x = e.clientX;
                }

                const delta = x - dragX;
                setScrollPosition(scrollPosition + delta);
                setDragX(x);
            }
        },
        [dragX, scrollPosition]
    );

    const onTrackDragEnd = useCallback(() => {
        if (dragX !== null) {
            const delta = initialDragX - dragX;
            const movedSlides = Math[delta > 0 ? 'ceil' : 'floor'](
                delta / slideWidth
            );
            let nextSlide = currentSlide + movedSlides;
            let position;

            if (infinite) {
                position = (-1 * nextSlide - slidesToAppend) * slideWidth;
            } else {
                if (nextSlide < 0) {
                    nextSlide = 0;
                } else if (nextSlide + slidesToShow > slideCount) {
                    nextSlide = slideCount - slidesToShow;
                }

                position = -1 * nextSlide * slideWidth;
            }

            setDisableTransition(false);
            setCurrentSlide(nextSlide);
            setScrollPosition(position);
            setInitialDragX(null);
            setDragX(null);
        }
    }, [
        slideWidth,
        currentSlide,
        initialDragX,
        dragX,
        slideCount,
        slidesToShow,
        infinite,
        slidesToAppend
    ]);

    const onSlideMove = useCallback(() => {
        let nextSlide = null;

        if (currentSlide < 0) {
            nextSlide = slideCount + currentSlide;
        } else if (currentSlide + slidesToShow > slideCount) {
            nextSlide = (slideCount - currentSlide) * -1;
        }

        if (nextSlide !== null) {
            moveToSlide(nextSlide, true);
        }
    }, [currentSlide, slideCount, slidesToShow, moveToSlide]);

    let sliderEvents;

    if (isMobileLayout) {
        sliderEvents = {
            onTouchStart: onTrackDragStart,
            onTouchMove: onTrackDrag,
            onTouchEnd: onTrackDragEnd,
            onTouchCancel: onTrackDragEnd
        };
    } else {
        sliderEvents = {
            onMouseDown: onTrackDragStart,
            onMouseMove: onTrackDrag,
            onMouseUp: onTrackDragEnd,
            onMouseLeave: onTrackDragEnd
        };
    }

    if (infinite) {
        sliderEvents.onTransitionEnd = onSlideMove;
    }

    useEffect(() => {
        let sliderW = sliderWidth;
        let slideW = slideWidth;

        if (!sliderWidth) {
            const { width } = slideContainerRef.current.getBoundingClientRect();
            setSliderWidth(width);
            sliderW = width;
            slideW = null;
        }

        if (!slideW) {
            slideW = sliderW / slidesToShow;
            setSlideWidth(slideW);

            if (infinite) {
                setDisableTransition(true);
                setScrollPosition(-1 * slidesToAppend * slideW);
                setTimeout(() => setDisableTransition(false), 0);
            }
        }

        if (destinationSlide !== goToSlide) {
            moveToSlide(goToSlide);
            setDestinationSlide(goToSlide);
        }
    }, [
        sliderWidth,
        slideWidth,
        slidesToShow,
        moveToSlide,
        destinationSlide,
        goToSlide,
        infinite,
        slidesToScroll,
        slidesToAppend
    ]);

    let slides;

    if (infinite) {
        slides = [
            ...repeatArray([...children].reverse(), slidesToAppend).reverse(),
            ...children,
            ...repeatArray(children, slidesToAppend)
        ].map((slide, i) => ({ slide, i: i - slidesToAppend }));
    } else {
        slides = children.map((slide, i) => ({ slide, i }));
    }

    return (
        <div className="slider">
            <div className="slide-container" ref={slideContainerRef}>
                <div
                    className={`slider-track ${
                        disableTransition ? 'disable-transition' : ''
                    }`}
                    style={{ transform: `translateX(${scrollPosition}px)` }}
                    {...sliderEvents}
                >
                    {slides.map(({ slide, i }) => (
                        <div
                            className="slide"
                            key={i}
                            data-key={i}
                            style={{ width: slideWidth }}
                        >
                            {slide}
                        </div>
                    ))}
                </div>
            </div>
            <div className="slide-arrows">
                <div
                    className={`arrow-left ${
                        !infinite && currentSlide <= 0 ? 'disabled' : ''
                    }`}
                    onClick={onLeftArrowClick}
                ></div>
                <div
                    className={`arrow-right ${
                        !infinite && currentSlide + slidesToShow >= slideCount
                            ? 'disabled'
                            : ''
                    }`}
                    onClick={onRightArrowClick}
                ></div>
            </div>
        </div>
    );
};

const SliderWrapper = props => {
    const [initParam, setInitParam] = useState(Date.now());
    const { slidesToShow, slidesToScroll, infinite, children } = props;
    const slideCount = children.length;

    useEffect(() => {
        setInitParam(Date.now());
    }, [slidesToShow, slidesToScroll, infinite]);

    return (
        <Slider
            {...props}
            slidesToShow={Math.min(slidesToShow, slideCount)}
            slidesToScroll={Math.min(slidesToScroll, slidesToShow, slideCount)}
            key={initParam}
        />
    );
};

SliderWrapper.propTypes = {
    slidesToShow: PropTypes.number.isRequired,
    slidesToScroll: PropTypes.number.isRequired,
    infinite: PropTypes.bool,
    goToSlide: PropTypes.number
};

SliderWrapper.defaultProps = {
    infinite: false,
    goToSlide: null
};

export default SliderWrapper;
