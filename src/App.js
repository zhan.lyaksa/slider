import React, { useCallback, useState } from 'react';

import { isMobile } from './utils';
import './App.css';
import Slider from './slider/Slider';

const App = () => {
    const [slide, setSlide] = useState(null);
    const [slidesToShow, setSlidesToShow] = useState(3);
    const [slidesToScroll, setSlidesToScroll] = useState(3);
    const [infinite, setInfinite] = useState(false);

    const onSelectChange = useCallback(
        ({ target }) => {
            setSlide(parseInt(target.value));
        },
        [setSlide]
    );

    const onSlidesToShowChange = useCallback(
        e => setSlidesToShow(parseInt(e.target.value)),
        []
    );

    const onSlidesToScrollChange = useCallback(
        e => setSlidesToScroll(parseInt(e.target.value)),
        []
    );

    const onInfiniteCheckboxChange = useCallback(
        e => setInfinite(e.target.checked),
        []
    );

    const items = Array(10).fill();
    const isMobileLayout = isMobile();

    return (
        <>
            <h1>Slider Demo</h1>
            <div className="demo-box">
                <p>
                    <label htmlFor="select-slide">Go to slide: </label>
                    <select
                        onChange={onSelectChange}
                        defaultValue="-1"
                        id="select-slide"
                    >
                        <option key={-1} value="-1" disabled></option>
                        {items.map((_, i) => (
                            <option key={i} value={i}>
                                {i + 1}
                            </option>
                        ))}
                    </select>
                </p>
                {!isMobileLayout && (
                    <>
                        <p>
                            <label htmlFor="set-slides-to-show">
                                Slides to show:
                            </label>
                            <input
                                type="text"
                                id="set-slides-to-show"
                                onChange={onSlidesToShowChange}
                                value={slidesToShow}
                            />
                        </p>
                        <p>
                            <label htmlFor="set-slides-to-scroll">
                                Slides to scroll:
                            </label>
                            <input
                                type="text"
                                id="set-slides-to-scroll"
                                onChange={onSlidesToScrollChange}
                                value={slidesToScroll}
                            />
                        </p>
                    </>
                )}
                <p>
                    <label>
                        <span>Infinite:</span>
                        <input
                            type="checkbox"
                            onChange={onInfiniteCheckboxChange}
                        />
                    </label>
                </p>
                <Slider
                    slidesToShow={isMobileLayout ? 1 : slidesToShow}
                    slidesToScroll={isMobileLayout ? 1 : slidesToScroll}
                    goToSlide={slide}
                    infinite={infinite}
                >
                    {items.map((_, i) => (
                        <div className="box" key={i}>
                            {i + 1}
                        </div>
                    ))}
                </Slider>
            </div>
        </>
    );
};

export default App;
